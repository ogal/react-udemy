// Importamos además la clase component
import React, {Component} from 'react';
import Projects from './Projects';
import Profiles from './Profiles';
import Title from './Title';

import profile from './assets/wanted-backend.jpg';

class App extends Component {

    constructor(){
        super();
        this.state = { displayBio: false };

        console.log('Component this', this);

        // this.readMore = this.readMore.bind(this);
        // this.showLess = this.showLess.bind(this);
        this.toggleDisplayBio = this.toggleDisplayBio.bind(this);
    }

    // readMore() {
    //     this.setState({ displayBio: true });
    // }

    // showLess(){
    //     this.setState({ displayBio: false });
    // }

    /**Aplicamos el valor contrario a lo que en ese momento esté aplicado 
     * Nunca modificamos directamente el estado del componente. Utilizaremos setState()
    */
    toggleDisplayBio = () => {
        this.setState({ displayBio: !this.state.displayBio });
    }

    render() {

        return(
            <div>
                <img src={ profile } alt="foto_perfil" className='foto_perfil' />
                <h1>Hola!</h1>
                <p>Soy Oscar</p>
                { this.state.displayBio ? <Title /> : null }
                {
                    this.state.displayBio ? (
                    <div>
                        <p>Vivo en Barcelona, y codifico todos los días</p>
                        <p>Mi lenguaje favorito es Javascript, y creo que React y Vue.js son geniales</p>
                        <p>A parte del código, tambén me gusta correr y viajar.</p>
                        <button onClick={this.toggleDisplayBio}>
                            Read less
                        </button>
                    </div>
                    ) : (<div>
                        <button onClick={this.toggleDisplayBio}>
                            Read more
                        </button>
                        </div>
                    )
                }

                <hr/>
                <Projects />
                <hr/>
                <Profiles />
            </div>
        )
    }
}

// Para que otros archivos puedan utilizar este componente
export default App;