import React, {Component} from 'react';

/**Importamos los datos que están en data */
import SPROFILES from './data/socialProfiles';

class Profile extends Component {
    render(){

        // console.log('this.props', this.props);
        const { name, link, image } = this.props.profile; 

        return (
            <div style={{ display: 'inline-block', width: 30, margin: 25 }}>
                <h5>{ name }</h5>        
                <a href={ link } target="_blank" rel="noopener noreferrer">
                    <img src={ image } alt='profile' style={{ width: 20, height: 20 }} />
                </a>
            </div>
        )
    }
}


class Profiles extends Component {

    render(){

        return(
            <div>
                <h2>Contacta conmigo!</h2>
                <div>
                    {
                        SPROFILES.map(SPROFILE => {
                            return (
                                <Profile key={SPROFILE.id} profile={SPROFILE} />
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Profiles;