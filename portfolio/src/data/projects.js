/** Importamos las tres imagenes, esto solo es informacion ( data ), no es un Componente, 
 * por eso el nombre del archivo empieza por minuscula y no en mayus.
 */
import project1 from '../assets/project1.png';
import project2 from '../assets/project2.png';
import project3 from '../assets/project3.png';

const PROJECTS = [
    {
        id: 1,
        title: 'Example React Aplication',
        description: 'My first React App',
        link: 'https://google.es',
        image: project1
    },
    {
        id: 2,
        title: 'My API',
        description: 'A REST API that I built from scratch with GET amd POST requests!',
        link: 'https://google.es',
        image: project2
    },
    {
        id: 3,
        title: 'Operating Systems Final Project',
        description: 'My unique final project for my university Operating Systems course.',
        link: 'https://google.es',
        image: project3
    }
];

export default PROJECTS;