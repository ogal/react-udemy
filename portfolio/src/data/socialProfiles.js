/**Importamos los iconos que necesitamos de la carpeta src/assets */
import emailIcon from '../assets/email_icon.png';
import githubIcon from '../assets/github_icon.png';
import linkedInIcon from '../assets/linkedin_icon.png';
import twitterIcon from '../assets/twitter_icon.png';

const SPROFILES = [
    {
        id: 1,
        name: 'Email',
        link: 'mailto:mailtest@gmail.com',
        image: emailIcon
    },
    {
        id: 2,
        name: 'GitHub',
        link: 'https://google.es',
        image: githubIcon
    },
    {
        id: 3,
        name: 'LinkedIn',
        link: 'https://google.es',
        image: linkedInIcon
    },
    {
        id: 4,
        name: 'Twitter',
        link: 'https://google.es',
        image: twitterIcon
    }
];

export default SPROFILES;
