import React, {Component} from 'react';


const TITLE = [
    'a software engineer',
    'an adventure sicker',
    'an enthusiastic learner',
    'esta es la troleada'
];

class Title extends Component {

    state = { titleIndex: 0, fadeIn: true };

    componentDidMount(){
        console.log('Title component has mounted');
        /** */
        this.timeout = setTimeout(() => this.setState({ fadeIn: false }), 2000);

        this.animateTitles();
    }

    componentWillUnmount(){
        console.log('Title component will unmount.');

        /**Con esta línea evitamos que se siga llamando al setState() despúes de haberlo desmontado, 
         * evitando con ello posibles fugas de recursos.
         */
        clearInterval(this.titleInterval);
        clearTimeout(this.timeout);
    }

    /**Funcion para recorrer los titulos del array, que llamamos en componentDidMount() */
    animateTitles = () => {
        /**Toma dos parametros, callback function to fire, velocidad en milisegundos */
        this.titleInterval =  setInterval( () => {
            const titleIndex = (this.state.titleIndex + 1) % TITLE.length;

            this.setState({ titleIndex, fadeIn: true });
            this.timeout = setTimeout(() => this.setState({ fadeIn: false }), 2000);
        }, 4000);

        console.log('this.titleInterval', this.titleInterval);
    }

    render(){

        const { fadeIn, titleIndex } = this.state;

        // Tomamos la constante para trabajar con ella:
        const title = TITLE[titleIndex];

        return(
            <p className={fadeIn ? 'title-fade-in' : 'title-fade-out' } >I am { title }</p>
        )

    }
}

export default Title;