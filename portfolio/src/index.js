import React from 'react';
import ReactDOM from 'react-dom';

// Componentes propios
import App from './App';
import './index.css';

/**Mostramos directamente un elemento en el html, sin componentes de por medio */
//ReactDOM.render(<div>React element</div>, document.getElementById('root'));

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<p>{animal1.name}</p>, document.getElementById('root'));

// class Animal {

//     constructor(name, age){
//         this.name = name; 
//         this.age = age;        
//     }

//     speak() {
//         console.log('I am ', this.name, ' and i am ', this.age, 'years old.');
//     }
// }

// const animal1 = new Animal('Turtle', 87);
// animal1.speak();


// class Lion extends Animal {

//     constructor(name, age, furColor, speed) {
//         super(name, age);
//         this.furColor = furColor;
//         this.speed = speed;
//     }

//     roar(){
//         console.log('RUUGIDO!', ' Soy ', this.name, ' y tengo ', this.age, ' años. Con pelo ', this.furColor, ' voy a ', this.speed, 'km/h.')
//     }
// }

// const leon1 = new Lion('Pandas', 3, 'Blanco', 46);
// leon1.roar();