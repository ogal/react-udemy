import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

import NameComponent from './components/NameComponent';
import ComponentePropio from './components/ComponentePropio';

class App extends Component {

  // Le decimos al componente que tendrá un estado: 
  constructor(props) {
    super(props);
    this.state = {
      estado: "Nuevo estado del componente",
      profession : 'Programmer'
    };
  }

  // Creamos un componente propio, este es útil para establecer defaults por ejemplo
  componentDidMount() {
    console.log('Se ha cargado App.js. Se han cargado los componentes y la app está lista.');
  }

  handleClick() {
    this.setState({
      estado : 'El estado ha sido modificado a golpe de click!'
    })
  }

  render() {

    // Podemos declarar el estado del componente como una constante de esta forma. 
    // const {estado} = this.state;

    var curso = "React for absolute beginers";
    var style = { fontSize: '35px' };

    var array = ['welcome', 'to', 'the', 'course'];
    const cadena = ['Pagani', 'Bugatti', 'Morgan', 'Maybach', 'Rimac'];

    var arrayRenderizado = [];

    // Debemos crear una key por cada uno de los valores si queremos que no nos de errores inesperados.
    array.forEach(function(word){
      arrayRenderizado.push(
        <p key={word} >{word}</p>
      )
    });

    return (
      <div>
        <p>Hola Mundo!</p>

        {/* No nos hace falta declarar la variable estado, la llamamos de esta forma ya que es estática */}
        <p>{ this.state.estado }</p>
        <p>{ this.state.profession }</p>
        <p className="text-large" style={ style }>Las dentro de render() deberemos especificarlas con Classname</p>
        <p style={{ fontSize: '2em' }}>Aplicando estilos dentro del método render()</p>
          <div>
            <p>{ curso }</p>

            { arrayRenderizado }

            <h4>Coches de lujo:</h4>
            { cadena.map(coche => {
              return (
                <p key={coche} >{coche}</p>
              )
            })}

            <NameComponent />
            <button onClick={() => { console.log('Hi') } } >
              <NameComponent />
            </button>
            <br/><br/>
            <button onClick={ this.componentDidMount } >
              Press!!
            </button>
            <ComponentePropio/>
            <br/><br/>
            <button onClick={ this.handleClick.bind(this) } >
              Apellido del usuario
            </button>
            <p>{ this.state.estado }</p>
            <NameComponent user_name='Victor Temprano' />
            <NameComponent />
          </div>
      </div>
    );
  }
}

export default App;
