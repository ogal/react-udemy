// Fix the bugs in this code and make it render without any errors.
import React, { Component } from 'react';

class ComponentePropio extends Component {

  componentDidMount() {
      console.log("Se ha montado el componente propio!");
  }

  iLikeFunctions() {
    console.log('yay functions');
  }

  render() {

    var array = ['here','we','go']

    const no = 'yes';
    const display = 'Ogaliz';

    return (

      <div>
        <p>{display}</p>
        <hr/>
        <input type="text" onChange={this.iLikeFunctions}/>
        <table>
          <tbody>
            {array.map((term, i) => {
              return (
                <tr key={ i }>
                  <td style={{ padding: '15px' }} key={ term }>{term}</td>
                  <td style={{ padding: '15px' }}>{no}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    );

  }
}

export default ComponentePropio;