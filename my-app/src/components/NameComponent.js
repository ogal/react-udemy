import React, { Component } from 'react';

class NameComponent extends Component {

  // Creamos un componente propio, este útil para establecer defaults por ejemplo
  componentDidMount() {
    console.log('Montado: mensaje desde NameComponent');
  }

  render() {

    var curso = "React for absolute beginers";
    const { user_name } = this.props;

    return (
      <div>
        <p>Oscar G cursando: { curso }</p>
        <p>El nombre del instructor es: { user_name ? user_name : 'no user here'}</p>
      </div>
    );
  }
}

export default NameComponent;
